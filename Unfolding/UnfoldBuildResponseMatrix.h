#ifndef __UnfoldBuildResponseMatrix__hh
#define __UnfoldBuildResponseMatrix__hh

#include "TString.h"

class TH1D;
class TH2D;
class TFile;

class UnfoldBuildResponseMatrix
{
 public:
  UnfoldBuildResponseMatrix();
  UnfoldBuildResponseMatrix(TString path, Float_t R, Float_t pTleading);
  ~UnfoldBuildResponseMatrix();
  
  void BuildDeltaPtResponseMatrix();
  void BuildGaussianResponseMatrix();
  
 private:
  Double_t SmearWithDeltaPt(Double_t pT);

 protected:
  TFile *fout;
  TFile *finput;

  TH1D *hntrue;
  TH1D *hdpT[13];
  TH2D *hResponse;

  Int_t nbins;
  Int_t nevts;
  
  Double_t pTmin;
  Double_t pTmax;

  static const Int_t nEmb=13;
};

#endif
