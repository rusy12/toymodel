#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TFile.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "Riostream.h"
#include "TStopwatch.h"
#include "TSVDUnfold.h"

#include "UnfoldSVD.h"

//======================================================================
UnfoldSVD::UnfoldSVD(Int_t kterm, TString outfile)
{
	fKterm=kterm;
	fout=new TFile(outfile.Data(), "UPDATE");
	fout->cd();
}

//======================================================================
UnfoldSVD::~UnfoldSVD()
{
  fout->Close();

  delete fout;
}

//==============================================================================
void UnfoldSVD::SetHistograms(TH1D *measured, TH1D* reco_prior, TH1D *prior, TH2D *response_matrix)
{
  //SET/CHANGE HISTOGRAM TITLES
  prior->SetTitle("MC input spectrum");
  prior->SetXTitle("p_{T}^{corr}");
  reco_prior->SetTitle("MC reconstructed spectrum");
  reco_prior->SetXTitle("p_{T}^{corr}");
  measured->SetTitle("measured data");
  measured->SetXTitle("p_{T}^{corr}");

  HPrior = (TH1D*)prior->Clone("HPrior");
  HRecoPrior = (TH1D*)reco_prior->Clone("HRecoPrior");
  HMeasured = (TH1D*)measured->Clone("HMeasured");
  HResponse = (TH2D*)response_matrix->Clone("HResponse");

  //Create covariance matrix
  
  HCovData =  (TH2D*)response_matrix->Clone("HCovData");
  HCovData->Reset("MICE");
  for (int i=1; i<=measured->GetNbinsX(); i++) {
      HCovData->SetBinContent(i,i,measured->GetBinError(i)*measured->GetBinError(i)); 
   }
	
if(fKterm==1){
  fout->mkdir("input");
  fout->cd("input");
  cout<<"saving input histograms"<<endl;
  HPrior->Write("hprior");
  HRecoPrior->Write("hMCreco");
  HResponse->Write("hresponse");
  HMeasured->Write("hmeasured");
  }
}

//==============================================================================
void UnfoldSVD::Unfold()
{
cout<<"starting unfolding"<<endl;
TSVDUnfold * unfolding = new TSVDUnfold(HMeasured, HCovData, HRecoPrior, HPrior, HResponse);
cout<<"unfolding"<<endl;
HUnfolded = unfolding->Unfold(fKterm);	
cout<<"D vector"<<endl;
HDvector = unfolding->GetD();
cout<<"Singular Values"<<endl;
HSingVal=unfolding->GetSV();
cout<<"making covariance matrix"<<endl;
   // Compute the error matrix for the unfolded spectrum using toy MC
   // using the measured covariance matrix as input to generate the toys
   // 100 toys should usually be enough
   // The same method can be used for different covariance matrices separately
cout<<"   from measured CM"<<endl;
HCovUnf = unfolding->GetUnfoldCovMatrix( HCovData, 100 );   
   // Now compute the error matrix on the unfolded distribution originating
   // from the finite detector matrix statistics
cout<<"   from RM"<<endl;
   TH2D* uadetcov = unfolding->GetAdetCovMatrix( 100 ); 
HCovUnf->Add(uadetcov);
//Get the computed regularized covariance matrix (always corresponding to total uncertainty passed in constructor) and add uncertainties from finite MC statistics. 
cout<<"   tau"<<endl;
   HCovTau = unfolding->GetXtau();
   HCovTau->Add( uadetcov );
cout<<"unfolding done"<<endl;

//set error bars
for(Int_t bin=1;bin <= HUnfolded->GetNbinsX(); bin++)
	{
	  Double_t error = TMath::Sqrt(HCovUnf->GetBinContent(bin, bin));
	  HUnfolded->SetBinError(bin, error);
	}

cout<<"writing output"<<endl;
fout->mkdir(Form("kterm%d", fKterm));                                                                              
fout->cd(Form("kterm%d", fKterm));
HDvector->Write("hdvector");
HUnfolded->Write("hunfolded");
HSingVal->Write("hSingVal");
HCovUnf->Write("hcovariance");
HCovTau->Write("hcovtau");
}
//==============================================================================


