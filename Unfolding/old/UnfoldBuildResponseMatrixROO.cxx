#include "TF1.h"
#include "TH2D.h"
#include "TFile.h"
#include "TMath.h"
#include "TRandom.h"
#include "TString.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TDirectoryFile.h"
 
#include "UnfoldBuildResponseMatrixROO.h"

//=============================================================================
UnfoldBuildResponseMatrixROO::UnfoldBuildResponseMatrixROO(Float_t R, Float_t pTthresh, Int_t priorNo, TString type, Float_t pTstart)
{
  path=gSystem->Getenv("RM_PATH");
  pyEmb_path = gSystem->Getenv("PYEMB_PATH");
  prior_path= gSystem->Getenv("PRIOR_PATH");
  true_path= gSystem->Getenv("TRUE_PATH");

  nbins = 800;
  nevts = 1E9;

  pTmax = 100.;
  pTmin = -pTmax;
	if(type=="effi"){
		pTmin=0;
		nbins=400;
	}
  
  hResponse = 0x0;
  mtype=type;

  TString prior_type[]={"truth","flat","pythia","powlaw3","powlaw4","powlaw5","powlaw6","levy","levy_alex"};
  str=Form("%s/response_matrix_%s_R%.1lf_pTlead%.0lf.root", path.Data(),mtype.Data(),R,pTthresh);
  if(mtype=="effi" || mtype=="dete")str=Form("%s/pythia_emb_R%.1lf.root",pyEmb_path.Data(),R);
  finput = new TFile(str, "OPEN");

      TString name = "hResponse_1E9"; //use "classical" RM as input
		if(mtype=="effi" || mtype=="dete") name = Form("hresponse_pTl%.0lf",pTthresh);
       hRMin = (TH2D*)finput->Get(name.Data());

	Int_t binzero=hRMin->GetYaxis()->FindBin(0.0);
	for(Int_t i=binzero; i<hRMin->GetNbinsY()+1; i++){
	   Int_t bin = i;
   	TString name=Form("hdpT_%i",i);
   	hdpT[i-binzero]=(TH1D*)hRMin->ProjectionX(name, bin, bin);
	}
  str = Form("%s/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", path.Data(),mtype.Data(),R,pTthresh,priorNo);
  if(mtype=="effi")str= Form("%s/rmatrix/response_matrix_%s_R%.1lf_pTlead%.1lf_prior%i.root", pyEmb_path.Data(),mtype.Data(),R,pTthresh,priorNo);
  fout = new TFile(str, "RECREATE");


//seting up prior distribution
if (priorNo==0){
str=Form("%s/histos_jets_R%.1lf_pTcut0.2.root",true_path.Data(),R);
cout<<"opening file: "<<str.Data()<<endl;
TFile *fprior = new TFile(str.Data(), "OPEN");
TH2D *hPtRecpTleadingPrior = (TH2D*)fprior->Get("fhPtRecpTleading");
  Int_t firstbin = hPtRecpTleadingPrior->GetXaxis()->FindBin(pTthresh);
  Int_t lastbin = hPtRecpTleadingPrior->GetNbinsX();
  hprior = hPtRecpTleadingPrior->ProjectionY("hprior", firstbin, lastbin);
}
else if (priorNo==2){
  str = Form("%s/histos_pythiajet_R%.1lf.root", prior_path.Data(),R);
  TFile *fprior = new TFile(str.Data(), "OPEN");
  TH2D *hPrior2d = (TH2D*)fprior->Get("hpT_pTlead");
  Int_t firstbin = hPrior2d->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = hPrior2d->GetNbinsY();
  hprior = (TH1D*)hPrior2d->ProjectionX("prior_pythia", firstbin, lastbin);
  }
else{
  str = Form("%s/histos_prior.root", prior_path.Data());
  TFile *fprior = new TFile(str.Data(), "OPEN");
  str=Form("hprior_%s",prior_type[priorNo].Data());
  TH2D *hprior2d = (TH2D*)fprior->Get(str.Data());
  Int_t firstbin = hprior2d->GetYaxis()->FindBin(pTthresh);
  Int_t lastbin = firstbin;
  TString priorName=Form("prior_%i",priorNo);
  hprior = hprior2d->ProjectionX(priorName,firstbin,lastbin,"e");
  }


if(pTstart>0){
TH1D* hprtmp=(TH1D*) hprior->Clone("hprtmp");
hprior->Reset("MICE");
Int_t firstbin=hprior->GetXaxis()->FindBin(pTstart);
for(int bn=firstbin;bn<hprior->GetNbinsX();bn++){
	hprior->SetBinContent(bn,hprtmp->GetBinContent(bn));
	hprior->SetBinError(bn,hprtmp->GetBinError(bn));
}
delete hprtmp;
}
/*
if(mtype=="dete" || mtype=="BG_dete"){
//jet reconstruction efficiency
  if(pTstart>0) str = Form("%s/epsilon_R%.1lf_pTlead%0.lf_pTstart%0.lf.root",path.Data(),R,pTthresh,pTstart);
  else  str = Form("%s/epsilon_R%.1lf_pTlead%0.lf.root",path.Data(),R,pTthresh);
  fepsilon= new TFile(str.Data(), "OPEN");
  hepsilon=(TH1D*) fepsilon->Get("hepsilon");
	}*/
}

//==============================================================================
UnfoldBuildResponseMatrixROO::~UnfoldBuildResponseMatrixROO()
{
  fout->Close();
  delete fout;
}

//==============================================================================
void UnfoldBuildResponseMatrixROO::BuildDeltaPtResponseMatrix()
{
  TString name;
  Int_t save_evt = 9;

  hResponse = new TH2D("hResponse", "hResponse;p_{T}^{meas};p_{T}^{true};entries", nbins, pTmin, +pTmax, nbins, pTmin, +pTmax);
  hMCtrue = new TH1D("hMCtrue", "hMCtrue;p_{T}^{true};entries", nbins, pTmin, +pTmax);
  hMCreco = new TH1D("hMCreco", "hMCreco;p_{T}^{reco};entries", nbins, pTmin, +pTmax);
  
/*hResponse_eff = new TH2D("hResponse_eff", "hResponse_eff;p_{T}^{meas};p_{T}^{true};entries", nbins, pTmin, +pTmax, nbins, pTmin, +pTmax);
  hMCreco_eff = new TH1D("hMCreco_eff", "hMCreco_eff;p_{T}^{reco};entries", nbins, pTmin, +pTmax);*/
/*hResponse_15 = new TH2D("hResponse_15", "hResponse_15;p_{T}^{meas};p_{T}^{true};entries", nbins, pTmin, +pTmax, nbins, pTmin, +pTmax);
  hResponse_15_eff = new TH2D("hResponse_15_eff", "hResponse_15_eff;p_{T}^{meas};p_{T}^{true};entries", nbins, pTmin, +pTmax, nbins, pTmin, +pTmax);
  hMCtrue_15 = new TH1D("hMCtrue_15", "hMCtrue_15;p_{T}^{true};entries", nbins, pTmin, +pTmax);
  hMCreco_15 = new TH1D("hMCreco_15", "hMCreco_15;p_{T}^{reco};entries", nbins, pTmin, +pTmax);
  hMCreco_15_eff = new TH1D("hMCreco_15_eff", "hMCreco_15_eff;p_{T}^{reco};entries", nbins, pTmin, +pTmax);*/
  
  for(Int_t ievt = 0; ievt < nevts; ievt++)
    {
      Double_t pT = hprior->GetRandom();
      Double_t dpT = SmearWithDeltaPt(pT);
		hMCtrue->Fill(pT);
   	hResponse->Fill(dpT, pT);
		hMCreco->Fill(dpT);
/*if(mtype=="dete" || mtype=="BG_dete"){

   	Double_t epsilon=hepsilon->GetBinContent(hepsilon->FindBin(pT));
      Double_t rnd=gRandom->Uniform(0,1);
      if(epsilon>rnd){
		//cout<<"epsilon "<<epsilon<<endl;
   	   hResponse_eff->Fill(dpT, pT);
			hMCreco_eff->Fill(dpT);
		}
		}*/
      if(ievt != TMath::Power(10, save_evt) - 1) continue;
      fout->cd();
      name = Form("hResponse_1E%d", save_evt);
      hResponse->Write(name.Data());
      name = Form("hMCtrue_1E%d", save_evt);
      hMCtrue->Write(name.Data());
      name = Form("hMCreco_1E%d", save_evt);
      hMCreco->Write(name.Data());
     /* name = Form("hResponse_1E%d_15", save_evt);
      hResponse_15->Write(name.Data());
      name = Form("hMCtrue_1E%d_15", save_evt);
      hMCtrue_15->Write(name.Data());
      name = Form("hMCreco_1E%d_15", save_evt);
      hMCreco_15->Write(name.Data());*/
	/*	if(mtype=="dete" || mtype=="BG_dete"){
	      name = Form("hResponse_1E%d_eff", save_evt);
	      hResponse_eff->Write(name.Data());
	      name = Form("hMCreco_1E%d_eff", save_evt);
	      hMCreco_eff->Write(name.Data());
	      name = Form("hResponse_1E%d_15_eff", save_evt);
	      hResponse_15_eff->Write(name.Data());
	      name = Form("hMCreco_1E%d_15_eff", save_evt);
	      hMCreco_15_eff->Write(name.Data());
		}*/
      cout << Form("Event 1E%d saved!", save_evt) << endl;
      save_evt++;
    }
  
  delete hResponse;
  delete hMCtrue;
  delete hMCreco;
/*
  delete hResponse_eff;
  delete hMCreco_eff;
  delete hResponse_15;
  delete hMCtrue_15;
  delete hMCreco_15;
  delete hResponse_15_eff;
  delete hMCreco_15_eff;
*/
}

//==============================================================================
Double_t UnfoldBuildResponseMatrixROO::SmearWithDeltaPt(Double_t pT)
{
   Double_t dpT = 0;
   Int_t  bin = hRMin->GetYaxis()->FindBin(pT);
   Int_t binzero=hRMin->GetYaxis()->FindBin(0.0);
  if (pT < 0)dpT=0;
  else dpT = hdpT[bin-binzero]->GetRandom();
  return dpT;

}
