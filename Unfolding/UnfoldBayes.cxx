#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TFile.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "Riostream.h"
#include "TStopwatch.h"

#include "UnfoldBayes.h"

//==============================================================================
UnfoldBayes::UnfoldBayes(Int_t niterations, TString outfile)
{
  fniterations = niterations;

  kSmooth = kFALSE;

  fout = new TFile(outfile.Data(), "recreate");
  gROOT->cd();
}

//==============================================================================
UnfoldBayes::~UnfoldBayes()
{
  fout->Close();

  delete fout;
}

//==============================================================================
void UnfoldBayes::SetHistograms(TH1D *measured, TH1D *prior, TH2D *smearing)
{
  HPrior = (TH1D*)prior->Clone("HPrior");
  HMeasured = (TH1D*)measured->Clone("HMeasured");
  HUnfolded = (TH1D*)prior->Clone("HUnfolded");

  HPECPrior = (TH2D*)smearing->Clone("HPECPrior");
  HSmearing = (TH2D*)smearing->Clone("HSmearing");
  HUnfolding = (TH2D*)smearing->Clone("HUnfolding");

  HCovUnfoldPois = (TH2D*)smearing->Clone("HCovUnfoldPois");
  HCovUnfoldMult = (TH2D*)smearing->Clone("HCovUnfoldMult");

  HCorrUnfoldPois = (TH2D*)smearing->Clone("HCovUnfoldPois");
  HCorrUnfoldMult = (TH2D*)smearing->Clone("HCovUnfoldMult");
  
  fncauses = HSmearing->GetNbinsX();
  fneffects = HSmearing->GetNbinsY();

  SetDimensions();
  SetMPEC();
  SetVPrior();
  SetVMeasured();
  
  fout->mkdir("input");
  fout->cd("input");

  HPrior->Write("prior0");
  HSmearing->Write("PEC");
  HMeasured->Write("hmeasured");

  gROOT->cd();    
}

//==============================================================================
void UnfoldBayes::SetHistograms(TH1D *truth, TH1D *measured, TH1D *prior, TH2D *smearing)
{
  HTruth = (TH1D*)truth->Clone("HTruth");
  HPrior = (TH1D*)prior->Clone("HPrior");
  HMeasured = (TH1D*)measured->Clone("HMeasured");
  HUnfolded = (TH1D*)prior->Clone("HUnfolded");

  HPECPrior = (TH2D*)smearing->Clone("HPECPrior");
  HSmearing = (TH2D*)smearing->Clone("HSmearing");
  HUnfolding = (TH2D*)smearing->Clone("HUnfolding");

  HCovUnfoldPois = (TH2D*)smearing->Clone("HCovUnfoldPois");
  HCovUnfoldMult = (TH2D*)smearing->Clone("HCovUnfoldMult");

  HCorrUnfoldPois = (TH2D*)smearing->Clone("HCovUnfoldPois");
  HCorrUnfoldMult = (TH2D*)smearing->Clone("HCovUnfoldMult");
  
  fncauses = HSmearing->GetNbinsX();
  fneffects = HSmearing->GetNbinsY();

  SetDimensions();
  SetMPEC();
  SetVPrior();
  SetVMeasured();
  
  fout->mkdir("input");
  fout->cd("input");

  HTruth->Write("htruth");
  HPrior->Write("hprior");
  HSmearing->Write("PEC");
  HMeasured->Write("hmeasured");

  gROOT->cd();    
}

//==============================================================================
void UnfoldBayes::SetDimensions()
{
  // VECTORS
  VPrior.ResizeTo(fncauses);
  VMeasured.ResizeTo(fneffects);
  VUnfolded.ResizeTo(fncauses);

  // MATRICES
  MPEC.ResizeTo(fncauses, fneffects);
  MPCE.ResizeTo(fncauses, fneffects);

  MCovMeasMult.ResizeTo(fneffects, fneffects);
  MCovMeasPois.ResizeTo(fneffects, fneffects);

  MCovUnfoldMult.ResizeTo(fncauses, fncauses);
  MCovUnfoldPois.ResizeTo(fncauses, fncauses);
}

//==============================================================================
void UnfoldBayes::Unfold()
{
  for(Int_t iteration = 0; iteration < fniterations; iteration++)
    {
      cout << "*************************************" << endl;
      cout << Form("Iteration number %d", iteration) << endl;

      HUnfolded->Reset("MICE");

      FillPECPrior();
      
      cout << "Unfolding..." << endl;
      MakeUnfoldingMatrix();
      
      for(Int_t causei = 0; causei < fncauses; causei++)
	VUnfolded(causei) = 0.;
      
      for(Int_t causei = 0; causei < fncauses; causei++)
	for(Int_t effectj = 0; effectj < fneffects; effectj++)
	  VUnfolded(causei) += VMeasured(effectj) * MPCE(causei, effectj);
      
      for(Int_t causei = 0; causei < fncauses; causei++)
	HUnfolded->SetBinContent(causei + 1, VUnfolded(causei));

      cout << "Calculating covariance matrix..." << endl;
      TStopwatch timer;
      timer.Start();
      MakeCovarianceMatrix();
      timer.Stop();
      cout << "done in: ";
      timer.Print();

      cout << "Calculating correlation matrix..." << endl;
      MakeCorrelationMatrix();
      
      
      cout << "Saving results..." << endl;
      SaveResults(iteration);

      cout << "Updating prior..." << endl;
      UpdatePrior();
      cout << "*************************************" << endl;
      cout << "" << endl;
    }
}

//==============================================================================
void UnfoldBayes::SetMPEC()
{
  // 1. We define Smearing matrix as R(fncauses, fneffects)
  //    R(Ci, Ej) = R(xtrue, xmeas)
  // 2. Each line is made by measured(smeared) values
  //    Ej in the Bayesian definition
  // 3. Each column is made by true values
  //    Ci un the Bayesian defintion
  // 4. Note this has the same definition as LAMBDA MATRIX in arXiv:1010.0632v1
  // 5. It has the same index convention as TH2D, but starting from zero
  //    however, the vertical axis "evolution" is top --> bottom, whereas
  //    in TH2D it is bottom --> top

  for(Int_t causei = 1; causei <= fncauses; causei++)
    for(Int_t effectj = 1; effectj <= fneffects; effectj++)
      MPEC(causei - 1, effectj - 1) = HSmearing->GetBinContent(causei, effectj);
}

//==============================================================================
void UnfoldBayes::FillPECPrior()
{
  HPECPrior->Reset("MICE");

  for(Int_t causei = 0; causei < fncauses; causei++)
    for(Int_t effectj = 0; effectj < fneffects; effectj++)
      HPECPrior->SetBinContent(causei + 1, effectj + 1, MPEC(causei, effectj)*VPrior(causei));
}

//==============================================================================
void UnfoldBayes::SetVPrior()
{
  for(Int_t causei = 0; causei < fncauses; causei++)
    VPrior(causei) = HPrior->GetBinContent(causei + 1);

  if(kSmooth)
    { 
      HPrior->GetXaxis()->SetRangeUser(fXmin, fXmax);
      HPrior->Smooth(3, "R");
      for(Int_t causei = 0; causei < fncauses; causei++) 
	VPrior(causei) = HPrior->GetBinContent(causei+1);
    }
}

//==============================================================================
void UnfoldBayes::UpdatePrior()
{
  for(Int_t causei = 0; causei < fncauses; causei++)
    {
      VPrior(causei) = 0.0;
      VPrior(causei) = VUnfolded(causei);
      HPrior->SetBinContent(causei+1, VUnfolded(causei));
    }

  if(kSmooth)
    { 
      HPrior->GetXaxis()->SetRangeUser(fXmin, fXmax);
      HPrior->Smooth(3, "R");
      for(Int_t causei = 0; causei < fncauses; causei++) 
	VPrior(causei) = HPrior->GetBinContent(causei+1);
    }
}

//==============================================================================
void UnfoldBayes::SetVMeasured()
{

  for(Int_t bin = 1; bin <= fneffects; bin++)
    VMeasured(bin - 1) = HMeasured->GetBinContent(bin);

  Double_t Ntotal = VMeasured.Sum();

  // Making cov(nEi, nEj) assuming multinomial distribution
  // Based on Frequentist Probability
  for(Int_t effi = 0; effi < fneffects; effi++)
    for(Int_t effj = effi; effj < fneffects; effj++)
      {
	if(effi == effj)
	  MCovMeasMult(effi, effj) = VMeasured(effi)*(1 - VMeasured(effi)/Ntotal);
	else
	  {
	    MCovMeasMult(effi, effj) = -VMeasured(effi)*VMeasured(effj)/Ntotal;
	    MCovMeasMult(effj, effi) = MCovMeasMult(effi, effj);
	  }
      }
  // Making cov(nEi, nEj) assuming poisson distribution
  // Based on Bayesian Probability
  for(Int_t effi = 0; effi < fneffects; effi++)
    MCovMeasPois(effi, effi) = VMeasured(effi) + 1;
}

//==============================================================================
void UnfoldBayes::MakeUnfoldingMatrix()
{
  // 1. We define Unfolding matrix as U(fncauses, fneffects)
  // 2. Each line is made by measured(smeared) values
  //    Ej in the Bayesian definition
  // 3. Each column is made by true values
  //    Ci un the Bayesian defintion
  for(Int_t causei = 0; causei < fncauses; causei++)
    for(Int_t effectj = 0; effectj < fneffects; effectj++)
      {
	MPCE(causei, effectj) = 0;
	
	Double_t sumcauses = 0.;
	
	for(Int_t causel = 0; causel < fncauses; causel++)
	  sumcauses += MPEC(causel, effectj)*VPrior(causel);
	
	if(sumcauses != 0)
	  MPCE(causei, effectj) =  MPEC(causei, effectj)*VPrior(causei) / sumcauses;
      }
  
  HUnfolding->Reset("MICE");

  for(Int_t causei = 0; causei < fncauses; causei++)
    for(Int_t effectj = 0; effectj < fneffects; effectj++)
      HUnfolding->SetBinContent(effectj + 1, causei + 1, MPCE(causei, effectj));
}

//==============================================================================
void UnfoldBayes::MakeCovarianceMatrix()
{
  // Creates covariance matrix as shown in http://en.wikipedia.org/wiki/Propagation_of_uncertainty
  // f = Ax  // Sigma_f = A Sigma_x A^T
  
  HCovUnfoldMult->Reset("MICE");
  HCovUnfoldPois->Reset("MICE");

  TMatrixD MTemp = TMatrixD(MCovUnfoldPois);

  // RESETING MATRIX
  MCovUnfoldPois.ResizeTo(fncauses, fncauses);
  MCovUnfoldMult.ResizeTo(fncauses, fncauses);

  // POISSSON
  MTemp.ResizeTo(fncauses, fncauses);
  //  SIGMA_x * A^T
  MTemp.MultT(MCovMeasPois, MPCE);
  // A * (SIGMA_x * A^T);
  MCovUnfoldPois.Mult(MPCE, MTemp);
  for(Int_t i = 0; i < fncauses; i++)
    for(Int_t j = i; j < fncauses; j++)
      {
	Double_t covij_pois = MCovUnfoldPois(i, j);
	HCovUnfoldPois->SetBinContent(i+1, j+1, covij_pois);
	HCovUnfoldPois->SetBinContent(j+1, i+1, covij_pois);
      }
 
  // MULTINOMIAL
  MTemp.ResizeTo(fncauses, fncauses);
  //  SIGMA_x * A^T
  MTemp.MultT(MCovMeasMult, MPCE);
  // A * (SIGMA_x * A^T);
  MCovUnfoldMult.Mult(MPCE, MTemp);
  for(Int_t i = 0; i < fncauses; i++)
    for(Int_t j = i; j < fncauses; j++)
      {
	Double_t covij_mult = MCovUnfoldMult(i, j);
	HCovUnfoldMult->SetBinContent(i+1, j+1, covij_mult);
	HCovUnfoldMult->SetBinContent(j+1, i+1, covij_mult);
      }


  if(0)
    {
      for(Int_t i = 0; i < fncauses; i++)
	for(Int_t j = i; j < fncauses; j++)
	  {
	    Double_t covij_mult = 0.0;
	    Double_t covij_pois = 0.0;
	    
	    for(Int_t k = 0; k < fneffects; k++)
	      for(Int_t l = 0; l < fneffects; l++)
		{
		  covij_mult += MPCE(i, k)*MCovMeasMult(k, l)*MPCE(j, l);
		  covij_pois += MPCE(i, k)*MCovMeasPois(k, l)*MPCE(j, l);
		}
	
	    MCovUnfoldPois(i, j) = covij_pois;
	    MCovUnfoldPois(j, i) = covij_pois;
	    HCovUnfoldPois->SetBinContent(i+1, j+1, covij_pois);
	    HCovUnfoldPois->SetBinContent(j+1, i+1, covij_pois);

	    MCovUnfoldMult(i, j) = covij_mult;
	    MCovUnfoldMult(j, i) = covij_mult;
	    HCovUnfoldMult->SetBinContent(i+1, j+1, covij_mult);
	    HCovUnfoldMult->SetBinContent(j+1, i+1, covij_mult);
	  }
    }
}

//==============================================================================
void UnfoldBayes::MakeCorrelationMatrix()
{
  HCorrUnfoldMult->Reset("MICE");
  HCorrUnfoldPois->Reset("MICE");
  
  for(Int_t i = 0; i < fncauses; i++)
    for(Int_t j = i; j < fncauses; j++)
      {
	if(MCovUnfoldPois(i,i) && MCovUnfoldPois(j,j))
	  {
	    Double_t rhoij = MCovUnfoldPois(i,j) / TMath::Sqrt(MCovUnfoldPois(i,i)*MCovUnfoldPois(j,j));

	    HCorrUnfoldPois->SetBinContent(i+1, j+1, rhoij);
	    HCorrUnfoldPois->SetBinContent(j+1, i+1, rhoij);
	  }

	if(MCovUnfoldMult(i,i) && MCovUnfoldMult(j,j))
	  {
	    Double_t rhoij = MCovUnfoldMult(i,j) / TMath::Sqrt(MCovUnfoldMult(i,i)*MCovUnfoldMult(j,j));

	    HCorrUnfoldMult->SetBinContent(i+1, j+1, rhoij);
	    HCorrUnfoldMult->SetBinContent(j+1, i+1, rhoij);
	  }
      }
}


//==============================================================================
void UnfoldBayes::SaveResults(Int_t iteration)
{
  fout->mkdir(Form("iter%d", iteration));
  fout->cd(Form("iter%d", iteration));
  
  MakeHistogram(HUnfolded);  
  MakeHistogram(HPrior);

  HUnfolded->Write("hunfolded");
  HUnfolding->Write("HPCE");
  HPrior->Write("hprior");

  HPECPrior->Write("HPECPrior");

  HCovUnfoldPois->Write("HCovariance_Poisson");
  HCovUnfoldMult->Write("HCovariance_Multinomial");

  HCorrUnfoldPois->Write("HCorrelation_Poisson");
  HCorrUnfoldMult->Write("HCorrelation_Multinomial");

  gROOT->cd();
}

//==============================================================================
void UnfoldBayes::MakeHistogram(TH1D *hist)
{
  TH1D *htemp = (TH1D*)hist->Clone("temp");
  hist->Reset("MICE");
  for(Int_t bin = 1; bin <= htemp->GetNbinsX(); bin++)
    {
      Double_t yield = htemp->GetBinContent(bin);
      Double_t error = TMath::Sqrt(yield);
      hist->SetBinContent(bin, yield);
      hist->SetBinError(bin, error);
    }
  delete htemp;
}
