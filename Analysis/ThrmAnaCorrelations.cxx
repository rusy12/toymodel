#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TString.h"
#include "THnSparse.h"
#include "TClonesArray.h"
#include "Riostream.h"

#include "ThrmJet.h"
#include "ThrmEmbedding.h"
#include "ThrmFourVector.h"
#include "ThrmAnaCorrelations.h"

//________________________________________________________
ThrmAnaCorrelations::ThrmAnaCorrelations(TString path, Double_t pTcut, Double_t radius)
{
  fradius = radius;
  TString str = "null";

  fakt_arr = new TClonesArray("ThrmJet", 40);
  fembedding_arr = new TClonesArray("ThrmEmbedding", 10);

  // INPUT FILE
  str = Form("%s/jets_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  fjets = new TFile(str.Data(), "OPEN");
  ftree = (TTree*)fjets->Get("RecoJets");
  ftree->SetBranchAddress("akt_jets", &fakt_arr);
  ftree->SetBranchAddress("rho", &frho);
  ftree->SetBranchAddress("embedding", &fembedding_arr);

  // OUTPUT FILE
  str = Form("%s/corr_histos_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  foutput = new TFile(str.Data(), "RECREATE");
  CreateHistos();
}

//________________________________________________________
ThrmAnaCorrelations::~ThrmAnaCorrelations()
{
  fjets->Close();
  delete fjets;

  delete fakt_arr;
  delete fembedding_arr;

  foutput->Close(); 
  delete foutput;
}

//________________________________________________________
void ThrmAnaCorrelations::CreateHistos()
{
  TString name;
  TString title;

  Int_t nbinsx = 4000;
  Float_t pTmin = -200;
  Float_t pTmax = +200;

  Int_t nbinsy = 100;
  Float_t Amin = 0.0;
  Float_t Amax = 1.0;

  // pTcorr X pTleading
  hpTcorrpTleading = new TH2D("hpTcorrpTleading", "hpTcorrpTleading;p_{T}^{leading} (GeV/c);p_{T}^{corr} (GeV/c);Entries", nbinsx/2, 0, pTmax, nbinsx, pTmin, pTmax);


  // pTcorr X Area
  hpTcorrArea = new TH2D("hpTcorrArea", "hpTcorrArea;p_{T}^{corr} (GeV/c);A_{jet} (sr);Entries", nbinsx, pTmin, pTmax, nbinsy, Amin, Amax);

  // delta pT X Area
  Int_t nemb = 6;
  Float_t pTemb[] = {0.01, 0.1, 1.0, 5.0, 10.0, 15.0};
  
  for(Int_t iemb = 0; iemb < nemb; iemb++)
    {
      name = Form("hdpt_pTemb%.1lf", pTemb[iemb]);
      title = Form("p_{T}^{emb} = %.1lf GeV/c;#deltap_{T} (GeV/c);A_{jet} (sr);Entries", pTemb[iemb]);
      hdpTArea[iemb] = new TH2D(name.Data(), title.Data(), nbinsx, pTmin, pTmax, nbinsy, Amin, Amax);
    }
  
  // pTcorr X pTleading X Area
  Int_t dim = 3;
  Int_t bins[] = {800, 200, 100};
  Double_t min[] = {-200,    0, 0};
  Double_t max[] = {+200, +200, 1};
  const Int_t nBinsLeadingTrackPt = 10;
  const Double_t binEdges[nBinsLeadingTrackPt+1] = {0.,1.,2.,3.,4.,5.,6.,8.,10.,12.,200.}; //store pT of leading track in jet
  
  hpTcorrpTleadingArea = new THnSparseF("hpTcorrpTleadingArea", "hpTcorrpTleadingArea; p_{T}^{corr} (GeV/c); p_{T}^{leading} (GeV/c); Area (sr)", dim, bins, min, max);
  //hpTcorrpTleadingArea->SetBinEdges(1,binEdges);
}

//________________________________________________________
void ThrmAnaCorrelations::RunAnalysis()
{
  Int_t Nentries = ftree->GetEntries();
  
  for(Int_t entry = 0; entry < Nentries; entry++)
    {
      if(entry%100 == 0)
	cout << Form("Event #%6d analyzed", entry) << endl;
      
      ftree->GetEntry(entry);
     
      FillJetHistos();
 
      FillDeltaPtHistos();
    }
  
  hpTcorrpTleadingArea->Write("hpTcorrpTleadingArea");
  foutput->Write();
}

//________________________________________________________
void ThrmAnaCorrelations::FillJetHistos()
{
  Int_t Njets = fakt_arr->GetEntries();
  Int_t goodjets = 0;
  
  for(Int_t ijet = 0; ijet < Njets; ijet++)
    {
      ThrmJet *jet = (ThrmJet*)fakt_arr->At(ijet);
      
      ThrmFourVector jetfv = jet->jet_fv;
      TLorentzVector jetlv = jetfv.GetTLorentzVector();
      
      Double_t pT = jetlv.Pt();
      Double_t pTleading = jet->pTleading;
      Double_t Area = jet->area;
      Double_t rho = frho;

      Double_t pTcorr = pT - rho*Area;
      
      Double_t corr[] = {pTcorr, pTleading, Area};
      
      hpTcorrArea->Fill(pTcorr, Area);
      hpTcorrpTleading->Fill(pTleading, pTcorr);
      hpTcorrpTleadingArea->Fill(corr);
    }
}

//________________________________________________________
void ThrmAnaCorrelations::FillDeltaPtHistos()
{
  Int_t nemb = fembedding_arr->GetEntries();
  ThrmEmbedding *embedding;
  
  for(Int_t iemb = 0; iemb < nemb; iemb++)
    {
      embedding = (ThrmEmbedding*)fembedding_arr->At(iemb);

      if(!embedding->ffoundJet) continue;
      
      Double_t dpT = embedding->fPtEmbReco - frho * embedding->fAreaEmb - embedding->fPtEmb;

      hdpTArea[iemb]->Fill(dpT, embedding->fAreaEmb);
    }
}
