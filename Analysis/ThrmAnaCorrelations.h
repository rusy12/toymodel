#ifndef __ThrmAnaCorrelations__hh
#define __ThrmAnaCorrelations__hh

class TH1D;
class TH2D;
class THnSparse;
class TFile;
class TTree;
class TNtuple;
class TClonesArray;

class ThrmAnaCorrelations
{
 public:
  ThrmAnaCorrelations(TString path, Double_t pTcut, Double_t radius);
  ~ThrmAnaCorrelations();

  void RunAnalysis();

 private:
  void CreateHistos();
  void FillJetHistos();
  void FillDeltaPtHistos();

 protected:
  Double_t fradius;
  Double_t frho;

  TH2D *hpTcorrArea;
  TH2D *hpTcorrpTleading;
  TH2D *hdpTArea[6];

  THnSparse *hpTcorrpTleadingArea;

  TFile *fjets;
  TFile *foutput;

  TTree *ftree;

  TClonesArray *fakt_arr;
  TClonesArray *fembedding_arr;
};

#endif
