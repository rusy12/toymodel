#ifndef __ThrmAnaHistos__hh
#define __ThrmAnaHistos__hh

#include "TString.h"

void RHICRecoilJetSpectrum(TString wrkdir = "~/Doutorado/toymodel/data/hjet/Mult2000");
void LHCRecoilJetSpectrum(TString wrkdir = "~/Doutorado/toymodel/data/hjet/Mult4800");

#endif
