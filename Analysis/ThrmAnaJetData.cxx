#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TString.h"
#include "TClonesArray.h"
#include "Riostream.h"

#include "ThrmJet.h"
#include "ThrmEmbedding.h"
#include "ThrmFourVector.h"
#include "ThrmAnaJetData.h"

//________________________________________________________
ThrmAnaJetData::ThrmAnaJetData(TString path, Double_t pTcut, Double_t radius)
{
  fradius = radius;
  TString str = "null";

  fakt_arr = new TClonesArray("ThrmJet", 40);
  fembedding_arr = new TClonesArray("ThrmEmbedding", 10);

  fAcut = 0.0;

  // INPUT FILE
  str = Form("%s/jets_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  fjets = new TFile(str.Data(), "OPEN");
  ftree = (TTree*)fjets->Get("RecoJets");
  ftree->SetBranchAddress("akt_jets", &fakt_arr);
  ftree->SetBranchAddress("rho", &frho);
  ftree->SetBranchAddress("embedding", &fembedding_arr);

  // OUTPUT FILE
  str = Form("%s/ana_data_jets_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  foutput = new TFile(str.Data(), "RECREATE");
  str = Form("Anti-kT Reco jets R=%.1lf pTcut = %.1lf", radius, pTcut);
  fjettuple = new TNtuple("jets", str.Data(), "pT:eta:phi:pTleading:Area:rho:njet");
  CreateHistos();
}

//________________________________________________________
ThrmAnaJetData::~ThrmAnaJetData()
{
  fjets->Close();
  delete fjets;

  delete fakt_arr;
  delete fembedding_arr;
  delete fjettuple;
  foutput->Close(); 
  delete foutput;
}

//________________________________________________________
void ThrmAnaJetData::CreateHistos()
{

  hevents= new TH1I("hevents","hevents",1,0,2); //number of events

  TString name;
  TString title;

  Int_t nbins = 4000;
  Float_t pTmin = -200;
  Float_t pTmax = +200;

  Int_t nemb = 13;
  Float_t pTemb[] = {0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 12.0, 15.0};

    for(Int_t iemb = 0; iemb < nemb; iemb++)
      {
	name = Form("hdpt_pTemb%.1lf", pTemb[iemb]);
	title = Form("p_{T}^{emb} = %.1lf GeV/c;#deltap_{T} (GeV/c);Probability", pTemb[iemb]);
	hdpT[iemb] = new TH1D(name.Data(), title.Data(), nbins, pTmin, pTmax);
      }
}

//________________________________________________________
void ThrmAnaJetData::RunAnalysis()
{
  Int_t Nentries = ftree->GetEntries();
  
  for(Int_t entry = 0; entry < Nentries; entry++)
    {
      if(entry%100 == 0)
	cout << Form("Event #%6d analyzed", entry) << endl;
      
      ftree->GetEntry(entry);
      
      FillJetNtuple();

      FillDeltaPtHistos();
  hevents->Fill(1);
    }

  foutput->Write();
}

//________________________________________________________
void ThrmAnaJetData::FillJetNtuple()
{
  Int_t Njets = fakt_arr->GetEntries();
  Int_t goodjets = 0;
  
  for(Int_t ijet = 0; ijet < Njets; ijet++)
    {
      ThrmJet *jet = (ThrmJet*)fakt_arr->At(ijet);
      
      ThrmFourVector jetfv = jet->jet_fv;
      TLorentzVector jetlv = jetfv.GetTLorentzVector();
      
      Double_t eta = jetlv.Eta();
      if(TMath::Abs(eta) > 1 - fradius) continue;
      Double_t Area = jet->area;
      if(Area < fAcut) continue;
      
      Double_t pT = jetlv.Pt();
      Double_t phi = jetlv.Phi(); 
      Double_t pTleading = jet->pTleading;
      Double_t rho = frho;
      
      fjettuple->Fill(pT, eta, phi, pTleading, Area, rho, goodjets);
      
      goodjets++;
    }
}
//________________________________________________________
void ThrmAnaJetData::FillDeltaPtHistos()
{
  Int_t nemb = fembedding_arr->GetEntries();
  ThrmEmbedding *embedding;
  
  for(Int_t iemb = 0; iemb < nemb; iemb++)
    {
      embedding = (ThrmEmbedding*)fembedding_arr->At(iemb);

      if(!embedding->ffoundJet) continue;
      
      Double_t dpT = embedding->fPtEmbReco - frho * embedding->fAreaEmb - embedding->fPtEmb;

      hdpT[iemb]->Fill(dpT);
    }
}
