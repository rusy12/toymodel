#ifndef __ThrmAnadNdpT__hh
#define __ThrmAnadNdpT__hh

#include "TH1D.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TClonesArray.h"

class ThrmAnadNdpT
{
 public:
  ThrmAnadNdpT(TString wrkdir);
  ~ThrmAnadNdpT();

  void RunInclusive();
  void RunHadronJet();
  
  void SetMultiplicity(Int_t mult) {fmult = mult;}

 private:
  void Terminate();

 protected:
  TFile *finput;
  TFile *foutput;

  TTree *ftreetoy;

  TH1D *fhdNdpT_jet;
  TH1D *fhdNdpT_boltz;
  
  TClonesArray *fpartarr;

  Int_t fmult;
};

#endif
