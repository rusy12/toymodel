#include "ThrmAnadNdpT.h"
#include "ThrmFourVector.h"

#include "Riostream.h"

//_____________________________________________________________________________
ThrmAnadNdpT::ThrmAnadNdpT(TString wrkdir)
{
  TString str;
  TString name;
  TString title;

  fmult = 0;

  // INPUT
  fpartarr = new TClonesArray("ThrmFourVector", 3000);
  str = Form("%s/toymodel_events.root", wrkdir.Data());
  finput = new TFile(str.Data(), "OPEN");
  ftreetoy = (TTree*)finput->Get("ToyModel");
  ftreetoy->SetBranchAddress("particles", &fpartarr);

  // OUTPUT
  Int_t nbins = 400;
  Double_t xmin = -200;
  Double_t xmax = +200;
  str = Form("%s/dNdpT.root", wrkdir.Data());
  foutput = new TFile(str.Data(), "RECREATE");

  name = Form("hdNdpT_jet");
  title = Form("hdNdpT_jet;p_{T} (GeV/c);Entries");
  fhdNdpT_jet = new TH1D(name.Data(), title.Data(), nbins, xmin, xmax);
  fhdNdpT_jet->Sumw2();

  name = Form("hdNdpT_boltz");
  title = Form("hdNdpT_boltz;p_{T} (GeV/c);Entries");
  fhdNdpT_boltz = new TH1D(name.Data(), title.Data(), nbins, xmin, xmax);
  fhdNdpT_boltz->Sumw2();
}


//_____________________________________________________________________________
ThrmAnadNdpT::~ThrmAnadNdpT()
{
  finput->Close();
  delete finput;
}

//_____________________________________________________________________________
void ThrmAnadNdpT::RunInclusive()
{
  Int_t nentries = ftreetoy->GetEntries();

  for(Int_t entry = 0; entry < nentries; entry++)
    {
       if(entry%1000 == 0)
	 cout << Form("Event # %d processed", entry) << endl;

      ftreetoy->GetEntry(entry);

      Int_t Nparticles = fpartarr->GetEntries();

      for(Int_t ipart = 0; ipart < Nparticles; ipart++)
	{
	  ThrmFourVector *particle = (ThrmFourVector*)fpartarr->At(ipart);
	  TLorentzVector lvector = particle->GetTLorentzVector();

	  Double_t pT = lvector.Pt();
	  fhdNdpT_jet->Fill(pT);
	  fhdNdpT_boltz->Fill(pT);
	}
    }

  Terminate();
}

//_____________________________________________________________________________
void ThrmAnadNdpT::RunHadronJet()
{
  Int_t nentries = ftreetoy->GetEntries();

  for(Int_t entry = 0; entry < nentries; entry++)
    {
       if(entry%1000 == 0)
	 cout << Form("Event # %d processed", entry) << endl;

      ftreetoy->GetEntry(entry);

      Int_t Nparticles = fpartarr->GetEntries();

      for(Int_t ipart = 0; ipart < fmult; ipart++)
	{
	  ThrmFourVector *particle = (ThrmFourVector*)fpartarr->At(ipart);
	  TLorentzVector lvector = particle->GetTLorentzVector();

	  Double_t pT = lvector.Pt();
	  fhdNdpT_boltz->Fill(pT);
	}

      for(Int_t ipart = fmult; ipart < Nparticles; ipart++)
	{
	  ThrmFourVector *particle = (ThrmFourVector*)fpartarr->At(ipart);
	  TLorentzVector lvector = particle->GetTLorentzVector();

	  Double_t pT = lvector.Pt();
	  fhdNdpT_jet->Fill(pT);
	}

    }

  Terminate();
}

//_____________________________________________________________________________
void ThrmAnadNdpT::Terminate()
{
  foutput->Write();

  delete fhdNdpT_jet;
  delete fhdNdpT_boltz;

  foutput->Close();
  delete foutput;
}

