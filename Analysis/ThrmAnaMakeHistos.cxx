#include "ThrmAnaMakeHistos.h"

//________________________________________________________
ThrmAnaMakeHistos::ThrmAnaMakeHistos(TString wrkdir, Float_t radius, Float_t pTcut)
{
  TString str;

  Int_t nbins = 4000;
  Double_t pTmin = -100;
  Double_t pTmax = +100;

  str = Form("%s/ana_data_jets_R%.1lf_pTcut%.1lf.root", wrkdir.Data(), radius, pTcut);
  finput = new TFile(str.Data(), "OPEN");
  fntuple = (TNtuple*)finput->Get("jets");
  ConnectLeaves();

  hevents=(TH1I*)finput->Get("hevents");
  
  str = Form("%s/histos_jets_R%.1lf_pTcut%.1lf.root", wrkdir.Data(), radius, pTcut);
  foutput = new TFile(str.Data(), "RECREATE");

  fhDirectSpectrum = new TH1D("hDirectSpectrum","hDirectSpectrum;p_{T}^{direct} (GeV/c);N_{jets}", nbins, pTmin, pTmax);
  fhDirectSpectrum->Sumw2();
 
  fhPtReco = new TH1D("hPtReco","hPtReco;p_{T} (GeV/c);N_{jets}", nbins, pTmin, pTmax);
  fhPtReco->Sumw2();

  fhDSpTleading = new TH2D("fhDSpTleading", "fhDSpTleading;p_{T}^{leading} (GeV/c);p_{T}^{direct} (GeV/c)", nbins, pTmin, pTmax, nbins, pTmin, pTmax);
  fhDSpTleading->Sumw2();  

  fhPtRecpTleading = new TH2D("fhPtRecpTleading", "fhPtRecpTleading;p_{T}^{leading} (GeV/c);p_{T}^{rec} (GeV/c)", nbins/2, 0, pTmax, nbins/2, 0, pTmax);
  fhPtRecpTleading->Sumw2();  

  fhjetarea = new TH1D("hjetarea","jet area",100,0,1);
  fhjetpTarea = new TH2D("hjetpTarea","jet pTmeasured vs area", 800, pTmin, pTmax,100,0,1);
  fhjetpTcorrArea = new TH2D("hjetpTcorrArea","jet pTreco vs area", 800, pTmin, pTmax,100,0,1);
  fhrho = new TH1D("hrho","rho",50,0,50);

  foutput->cd();
  hevents->Write();
}

//________________________________________________________
ThrmAnaMakeHistos::~ThrmAnaMakeHistos()
{
  finput->Close();

  delete finput;
  delete foutput;
}

//________________________________________________________
void ThrmAnaMakeHistos::ConnectLeaves()
{
  fntuple->SetBranchAddress("pT", &pT);
  fntuple->SetBranchAddress("rho", &rho);
  fntuple->SetBranchAddress("Area", &Area);
  fntuple->SetBranchAddress("pTleading", &pTleading);
}

//________________________________________________________
void ThrmAnaMakeHistos::FillHistos()
{
  
  Int_t njets = fntuple->GetEntries();
  

  for(Int_t ijet = 0; ijet < njets; ijet++)
    {
      fntuple->GetEntry(ijet);
		if(ijet==0) fhrho->Fill(rho);
      Double_t pTdirect = pT - rho * Area;
      fhDirectSpectrum->Fill(pTdirect);
      fhPtReco->Fill(pT);

      fhDSpTleading->Fill(pTleading, pTdirect);
      fhPtRecpTleading->Fill(pTleading, pT);

		fhjetarea->Fill(Area);
		fhjetpTarea->Fill(pT,Area);
		fhjetpTcorrArea->Fill(pTdirect,Area);
    }
  
  foutput->cd();

  fhDirectSpectrum->Write();
  
  fhPtReco->Write();
  
  fhDSpTleading->Write();
  fhPtRecpTleading->Write();
	
	fhrho->Write();
	fhjetarea->Write();
	fhjetpTarea->Write();
	fhjetpTcorrArea->Write();


  foutput->Close();
}
