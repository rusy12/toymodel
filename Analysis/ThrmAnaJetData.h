#ifndef __ThrmAnaJetData__hh
#define __ThrmAnaJetData__hh

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TClonesArray.h"

class TH1D;
class TH2D;
class TFile;
class TTree;
class TNtuple;
class TClonesArray;

class ThrmAnaJetData
{
 public:
  ThrmAnaJetData(TString path, Double_t pTcut, Double_t radius);
  ~ThrmAnaJetData();

  void SetAcut(Double_t Acut) {fAcut = Acut;}
  void RunAnalysis();

 private:
  void FillJetNtuple();
  void FillDeltaPtHistos();
  void CreateHistos();

 protected:
  Double_t fradius;
  Double_t frho;
  Double_t fAcut;

  TH1D *hdpT[20];
  TH1I *hevents;

  TFile *fjets;
  TFile *foutput;

  TTree *ftree;

  TNtuple *fjettuple;

  TClonesArray *fakt_arr;
  TClonesArray *fembedding_arr;
};

#endif
