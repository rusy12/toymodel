#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TClonesArray.h"

#include "Riostream.h"

#include "ThrmEmbedding.h"
#include "ThrmAnaDeltaPtArea.h"

//_____________________________________________________________________________
ThrmAnaDeltaPtArea::ThrmAnaDeltaPtArea(TString path, Double_t pTcut, Double_t radius)
{
  TString str = "null";
  
  // INPUT FILE
  str = Form("%s/jets_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  finput = new TFile(str.Data(), "OPEN");
  ftree = (TTree*)finput->Get("RecoJets");
  ftree->SetBranchStatus("*", 0); // deactivating all branches
  ftree->SetBranchStatus("embedding*", 1); // activating only embedding
  ftree->SetBranchAddress("embedding", &fembedding_arr);

  // OUTPUT FILE
  str = Form("%s/histos_dpTarea_R%.1lf_pTcut%.1lf.root", path.Data(), radius, pTcut); 
  foutput = new TFile(str.Data(), "RECREATE");

  CreateHistos();
}

//_____________________________________________________________________________
ThrmAnaDeltaPtArea::~ThrmAnaDeltaPtArea()
{
  finput->Close();
  delete finput;

  foutput->Close();
  delete foutput;
}

//_____________________________________________________________________________
void ThrmAnaDeltaPtArea::CreateHistos()
{
  Int_t nemb = 6;
  Float_t pTemb[] = {0.01, 0.1, 1.0, 5.0, 10.0, 15.0};

  TString name;
  TString title;

  for(Int_t iemb = 0; iemb < nemb; iemb++)
    {
      // dpT
      name = Form("hdpT_pTemb%.1lf", pTemb[iemb]);
      title = Form("%s;#deltap_{T} (GeV/c); Entries", name.Data());
      fhdpT[iemb] = new TH1D(name.Data(), title.Data(), 200, -100, +100);

      // dpT Area > 0.4sr
      name = Form("hdpT_Acut_pTemb%.1lf", pTemb[iemb]);
      title = Form("%s;#deltap_{T} (GeV/c); Entries", name.Data());
      fhdpTAcut[iemb] = new TH1D(name.Data(), title.Data(), 200, -100, +100);

      // dpT X Area
      name = Form("hdpT_area_pTemb%.1lf", pTemb[iemb]);
      title = Form("%s;#deltap_{T} (GeV/c); Area (sr)", name.Data());
      fhdpTArea[iemb] = new TH2D(name.Data(), title.Data(), 200, -100, +100, 100, 0, 1);
    }
}

//_____________________________________________________________________________
void ThrmAnaDeltaPtArea::FillHistos()
{
  Int_t nentries = ftree->GetEntries();

  ThrmEmbedding *embedding;
  
  for(Int_t entry = 0; entry < nentries; entry++)
    {
      ftree->GetEntry(entry);

      Int_t nemb = fembedding_arr->GetEntries();

      for(Int_t iemb = 0; iemb < nemb; iemb++)
	{
	  embedding = (ThrmEmbedding*)fembedding_arr->At(iemb);

	  if (!embedding->ffoundJet) continue;

	  Double_t dpT = embedding->fdeltapT;
	  Double_t Area = embedding->fAreaEmb;

	  fhdpT[iemb]->Fill(dpT);
	  fhdpTArea[iemb]->Fill(dpT, Area);
	  
	  if (Area > 0.4) fhdpTAcut[iemb]->Fill(dpT);
	}
    }
  foutput->Write();
}
