#!/bin/csh

starver SL15e
source ../set_paths.csh


set BASEDIR="$HOME/jet_analysis/toymodel"
set MacroDir="$BASEDIR/macros"

setenv INPUTDIR $OUTPUTDIR
setenv WRKDIR $OUTPUTDIR

cd $MacroDir
if ( $ANAONLY == 0 ) then
root -b -q -l run_pythia.C  #2>&1
root -b -q -l make_dNdpT.C  #2>&1
root -b -q -l rec_jets.C  #2>&1
root -b -q -l make_ntuple.C  #2>&1
endif
root -b -q -l make_histos.C  #2>&1
root -b -q -l make_histos_deltapT.C #2>&1
