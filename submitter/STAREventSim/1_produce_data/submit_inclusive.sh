#!/bin/bash

source set_paths.sh
LOGDIR=$TOYMODELDIR/submitter/log
ERRDIR=$TOYMODELDIR/submitter/err
BASEDIR=$TOYMODELDIR
MACRODIR="$TOYMODELDIR/macros/STAREventSim/1_produce_data"
MACROFILE1="run_pythia.C"
MACROFILE2="make_dNdpT.C"
MACROFILE3="rec_jets.C"
MACROFILE4="make_ntuple.C"
MACROFILE5="make_histos.C"
MACROFILE6="make_histos_deltapT.C"

export USESCRATCH=1 # 1:use $SCRATCH for output

KEVENTS=400000 #total number of events, in thousands
export NEVENTS=50000 # 25000; how many events per job
export COLLIDER="RHIC_CHARGED" # RHIC | RHIC_CHARGED | LHC
OUTDIR="DataOut" #output directory name
export PTCUT=0.2 #min particle pT
export PTMAX=30 #max particle pT
export JETONLY=1 #only hard jets, if JETONLY=0 and BOLTZMAN=0 => generate hard jets + BG
export BOLTZMANN=0 #only soft BG
export BKGDPTCUT=0.2 #not used anymore
export HARDJET_TYPE=2 # hard jet spectrum distribution: 0: pT^-6 | 1: unbiased full PYTHIA R=0.6 (double Levy fit) | 2: unbiased full PYTHIA R=0.6 (Tsalis fit)
export PTMINHARD=4 #start of the hard distribution, default: 4GeV/c
EFFICORR=0 #simulate tracking inefficiency
PTSMEAR=0 #track pT smearing
export EFFTYPE="AuAu" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies
export DOSCALE=0 #rescale charge jets to the momentum of created full jet: 0: do not scale | 1: scale momentum of charged jet constituents | 2: create full jet with 3/2*pT
export RAAPTDEP=1 #pT dependent RAA - RAA starts at 0.2 and then slowly rises (upt to pT=15GeV)  to the value of "RAA"
export JETFRAG="u" #jet fragmentation: u | g | sp
export TOFEFFI=0; #apply TOF efficiency as one of the detector effects (this is used only for pp RM production)
#export CUTSET=0 #cutset used for tracking efficiency calculation (0=primary tracks,1=primary tracks,2=global tracks)
export MOMRES=1 #TPC momentum resolution; 0: sigma=0.01*pT^2 (global tracks) 1: sigma=0.005*pT^2 (primary tracks, simple) 2: sigma=a+b*pT+c*pT^2 (primary tracks, more accurate)
export MEANPT=600 #Background <pT> [MeV/c]
MULTIPLICITY=650 #Background multiplicity
export FIXEDSEED=1 #1: use SEED as a value for the random number generator's seed 0: use computer time as a seed
export SEED=42 #seed for random number generator
EVO="GPC2" #dataset/cutset label

JET_TYPE="pythia_$JETFRAG" # used for the output path
if [ $JETFRAG == "sp" ]; then
	JET_TYPE="sp"
fi
for CENTRAL in 1 #central or peripheral collisions
do
export CENTRAL 
if [ $CENTRAL -eq 1 ]; then
	RAA=0.5
else
	RAA=0.7
	RAAPTDEP=0
fi
export RAA

if [ $RAAPTDEP -eq 1 ]; then
	RAATYPE="varRAA"
else
	RAATYPE="RAA"
fi

if [ $CENTRAL -eq 1 ]; then
CSUF=""
else
CSUF="_peripheral"
fi

for RADIUS in 0.2 0.3 0.4 
do
export RADIUS 

TYPE4="_${EVO}${CSUF}_${RAATYPE}${RAA}_pTmax${PTMAX}_pTHARD${PTMINHARD}_${JETFRAG}_meanpT${MEANPT}_mult${MULTIPLICITY}" #output directory suffix


	if [ $JETONLY -eq 1 ]; then
		EVENT_TYPE="jetonly"
	elif [ $BOLTZMANN -eq 1 ]; then
		EVENT_TYPE="boltzman"
	else
		EVENT_TYPE="jet_plus_bg" 
	fi

   if [ $RADIUS == "0.2" ]; then
      ACUT=0.09
   elif [ $RADIUS == "0.3" ]; then
      ACUT=0.2
   elif [ $RADIUS == "0.4" ]; then
      ACUT=0.4
	elif [ $RADIUS == "0.5" ]; then
      ACUT=0.65
   fi

#if ([ $JETONLY -eq 1 ] || [ $BOLTZMANN -eq 1 ]); then
	#EFFICORR=0 
	#PTSMEAR=0 
	#ACUT=0
#fi
export ACUT
export EFFICORR
export PTSMEAR

echo "Area cut: $ACUT"

	if [ $HARDJET_TYPE -eq 0 ]; then
	TYPE1="${KEVENTS}k_charged_R${RADIUS}_A${ACUT}_powlaw"
	elif [ $HARDJET_TYPE -eq 1 ]; then
	TYPE1="${KEVENTS}k_charged_R${RADIUS}_A${ACUT}_levy"
	elif [ $HARDJET_TYPE -eq 2 ]; then
	TYPE1="${KEVENTS}k_charged_R${RADIUS}_A${ACUT}_tsalis"
	fi

	if [ $EFFICORR -eq 1 ]; then
	TYPE2="_effcorr"
	else
	TYPE2=""
	fi

	if [ $PTSMEAR -eq 1 ]; then
	TYPE3="_pTsmear"
	else
	TYPE3=""
	fi

	TYPE=${TYPE1}${TYPE2}${TYPE3}${TYPE4}


	if [ $CENTRAL -eq 1 ]; then
     NBIN=955 #0-10% centrality
   else
	  NBIN=20 #60-80%	
	fi

   export NBIN
	export SIGMA_NBIN=0
	export MULTIPLICITY
	export SIGMA_MULTIPLICITY=0
  
	export WRKDIR="${BASEDIR}/$OUTDIR/${JET_TYPE}/${EVENT_TYPE}/${TYPE}"
 
  	if [ ! -e $WRKDIR ]; then
		mkdir -p $WRKDIR
	fi

	if [ ! -e tmp ]; then
		mkdir -p tmp
	fi

	TEMPLATE_NAME="toy_inclusive_${CENTRAL}_R${RADIUS}.xml"
	NJOBS=$(( KEVENTS * 1000 / NEVENTS ))

#===========================
#create submission xml file
#===========================
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" > tmp/$TEMPLATE_NAME
echo "<job nProcesses=\"$NJOBS\" simulateSubmission = \"false\" >" >> tmp/$TEMPLATE_NAME
echo "<command>" >> tmp/$TEMPLATE_NAME
echo setenv USESCRATCH $USESCRATCH >> tmp/$TEMPLATE_NAME
echo setenv COLLIDER $COLLIDER >> tmp/$TEMPLATE_NAME
echo setenv PTCUT $PTCUT >> tmp/$TEMPLATE_NAME
echo setenv PTMAX $PTMAX >> tmp/$TEMPLATE_NAME
echo setenv JETONLY $JETONLY >> tmp/$TEMPLATE_NAME
echo setenv BOLTZMANN $BOLTZMANN >> tmp/$TEMPLATE_NAME
echo setenv BKGDPTCUT $BKGDPTCUT >> tmp/$TEMPLATE_NAME
echo setenv HARDJET_TYPE $HARDJET_TYPE >> tmp/$TEMPLATE_NAME
echo setenv PTMINHARD $PTMINHARD >> tmp/$TEMPLATE_NAME
echo setenv EFFTYPE $EFFTYPE >> tmp/$TEMPLATE_NAME
echo setenv EFFPATH $EFFPATH >> tmp/$TEMPLATE_NAME
echo setenv EFF_INCREMENT $EFF_INCREMENT >> tmp/$TEMPLATE_NAME
echo setenv TOFEFFI $TOFEFFI >> tmp/$TEMPLATE_NAME
echo setenv FIXEDSEED $FIXEDSEED >> tmp/$TEMPLATE_NAME
echo setenv SEED $SEED >> tmp/$TEMPLATE_NAME
echo setenv MOMRES $MOMRES >> tmp/$TEMPLATE_NAME
echo setenv DOSCALE $DOSCALE >> tmp/$TEMPLATE_NAME
echo setenv RAAPTDEP $RAAPTDEP >> tmp/$TEMPLATE_NAME
echo setenv JETFRAG $JETFRAG >> tmp/$TEMPLATE_NAME
echo setenv MEANPT $MEANPT >> tmp/$TEMPLATE_NAME
echo setenv CENTRAL $CENTRAL >> tmp/$TEMPLATE_NAME
echo setenv RAA $RAA >> tmp/$TEMPLATE_NAME
echo setenv RADIUS $RADIUS >> tmp/$TEMPLATE_NAME
echo setenv ACUT $ACUT >> tmp/$TEMPLATE_NAME
echo setenv EFFICORR $EFFICORR >> tmp/$TEMPLATE_NAME
echo setenv PTSMEAR $PTSMEAR >> tmp/$TEMPLATE_NAME
echo setenv NEVENTS $NEVENTS >> tmp/$TEMPLATE_NAME
echo setenv NBIN $NBIN >> tmp/$TEMPLATE_NAME
echo setenv SIGMA_NBIN $SIGMA_NBIN >> tmp/$TEMPLATE_NAME
echo setenv MULTIPLICITY $MULTIPLICITY >> tmp/$TEMPLATE_NAME
echo setenv SIGMA_MULTIPLICITY $SIGMA_MULTIPLICITY >> tmp/$TEMPLATE_NAME
echo setenv WRKDIR $WRKDIR >> tmp/$TEMPLATE_NAME
echo "  starver $STARLIB_VER" >> tmp/$TEMPLATE_NAME
echo "  source $ANALYSISDIR/set_paths.csh" >> tmp/$TEMPLATE_NAME
echo "  cd $MACRODIR"
echo "  pwd" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE1" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE2" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE3" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE4" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE5" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l $MACROFILE6" >> tmp/$TEMPLATE_NAME
echo "  </command>" >> tmp/$TEMPLATE_NAME
echo "  <stdout URL=\"file:$LOGDIR/\$JOBID.log\"/>" >> tmp/$TEMPLATE_NAME
echo "  <stderr URL=\"file:$ERRDIR/\$JOBID.err\"/>" >> tmp/$TEMPLATE_NAME
echo "  <output fromScratch=\"*histos*.root\" toURL=\"file:$WRKDIR/\"/>" >> tmp/$TEMPLATE_NAME
echo "  <SandBox>" >> tmp/$TEMPLATE_NAME
echo "    <Package>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE1</File>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE2</File>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE3</File>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE4</File>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE5</File>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$MACRODIR/$MACROFILE6</File>" >> tmp/$TEMPLATE_NAME
echo "			  </Package>" >> tmp/$TEMPLATE_NAME
echo "			  </SandBox>" >> tmp/$TEMPLATE_NAME
echo "			  </job>" >> tmp/$TEMPLATE_NAME

#let's submit
	cd tmp
	star-submit $TEMPLATE_NAME 
	cd ..


done #R
done #central
