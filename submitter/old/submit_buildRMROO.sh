#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp|BGD|dete|effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp|BG_pyt|dete|BG_dete|effi - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects, for efficiency calculation
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

BASEDIR="$HOME/jet_analysis/toymodel"
LOGDIR="$BASEDIR/submitter/log"
JET_TYPE="pythia" # pythia | sp
#JET_TYPE="sp" # pythia | sp
export WORKDIR="$BASEDIR/macros/response_matrix"

for CENTRAL in 1
do

if [ $CENTRAL -eq 1 ]; then
	CENTSUFF=""
	PTLEADCUTS="5 6 7"
else
	CENTSUFF="_peripheral"
	PTLEADCUTS="4 5 6"
fi

for RPARAM in 0.3 #0.3 0.4 0.5
do
for SYSSUF in  "_normal" #"_AuAu" #"_2u1g" "_m5" "_p5" "_normal" 
do
export RPARAM
export RM_PATH="$BASEDIR/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}${CENTSUFF}/rmatrix${SYSSUFF}" #path to input response matrix
export PRIOR_PATH="$HOME/jet_analysis/STARJet/out/prior"
export TRUE_PATH="$BASEDIR/DataOut/$JET_TYPE/jetonly/charged_R${RPARAM}${CENTSUFF}"
export PYEMB_PATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${SYSSUF}"
export RMATRIX_TYPE

for PTTHRESH in `echo $PTLEADCUTS` #pT leading cut
   do
   export PTTHRESH
	if [ "$RMATRIX_TYPE" == "effi" ]; then
	  	for PRIOR in 3 
     	do
   		export NAME="RM_R${RPARAM}_${PTTHRESH}_$PRIOR"
     		export PRIOR
				qsub -P star -l h_vmem=2G -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRM_ROO.sh
			done
	else
	  	for PRIOR in 2 4 5 6 7 8 9 10 11 12 13 14 15
     	do
   		export NAME="RM_R${RPARAM}_${PTTHRESH}_$PRIOR"
      	export PRIOR
			qsub -P star -l h_vmem=2G  -l h_rt=02:00:00 -N $NAME -o $LOGDIR -e $LOGDIR -V run_buildRM_ROO.sh
			#cd $WORKDIR
			#root -l -b -q buildResponseROO.C 2>&1
   	done
	fi
done
done
done
done
