#!/bin/bash
BASEDIR="$HOME/jet_analysis/toymodel"
LOGDIR="$BASEDIR/submitter/log"

kEVENTS=2000

export PTCUT=0.2
export MAXRAP=1.0
export CHARGED=1 #charged jets only
export EFFICORR=1 #apply tracking efficiency 
export PTSMEAR=1 #track pT smearing
export MOMRES=2 #TPC momentum resolution; 0: sigma=0.01*pT^2 (global tracks) 1: sigma=0.005*pT^2 (primary tracks, simple) 2: sigma=a+b*pT+c*pT^2 (primary tracks, more accurate) 3: sigma=0.003*pT^2 (for sys uncertainty)
export EFFTYPE="AuAu" #tracking efficiency model: pp | AuAu - like
#export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies, [-100%,100%]
export TOFEFFI=0 #correc also for TOF+BEMC maching efficiency (for pp study)
export TOFEFF_INCREMENT=0 #increase/decrease TOF+BEMC matching efficiency for systematic studies
export JETFRAG="2u1g" #jet fragmentation: u | g | 2u1g
export CUTSET=1 #set of track cuts which we are using (see ~/jet_analysis/STARJet/Analysis/Embedding/out_cutsetX/cuts.h for details on cuts), cutset=3: same as cutset 1, but for global tracks instead of primary tracks
export DOSCALE=0 #rescale charge jets to the momentum of created full jet
#SUFFIX="_TOF${TOFEFF_INCREMENT}_pp"
SUFFIX=""

for CENTRAL in 0 1 #central or peripheral collisions
do
	export CENTRAL

for RPARAM in 0.2 0.3 0.4 0.5 
do
export RPARAM

   if [ $RPARAM == "0.2" ]; then
      ACUT=0.07
   fi
   if [ $RPARAM == "0.3" ]; then
      ACUT=0.2
   fi
   if [ $RPARAM == "0.4" ]; then
      ACUT=0.4
   fi
   if [ $RPARAM == "0.5" ]; then
      ACUT=0.65
   fi

export AREACUT=$ACUT
echo "Area cut: $AREACUT"

	if [ $EFFICORR -eq 1 ]; then
   TYPE1="_effcorr"
   else
   TYPE1=""
   fi

   if [ $PTSMEAR -eq 1 ]; then
   TYPE2="_pTsmear"
   else
   TYPE2=""
   fi

	if [ $CENTRAL -eq 0 ]; then
	CENTRALITY="_peripheral"
	else
	CENTRALITY=""
	fi

TYPE="pyEmb_${kEVENTS}k_charged_R${RPARAM}${TYPE1}${TYPE2}${CENTRALITY}_${JETFRAG}_eff${EFF_INCREMENT}_${EFFTYPE}_trcuts${CUTSET}_momRes${MOMRES}${SUFFIX}"

export NEVENTS=100000 #number of events per job
   START=0
   export NJOBS=$(( kEVENTS * 1000 / NEVENTS ))
   #START=75
   #NJOBS=100
   echo "number of jobs: $NJOBS"

for((run=START; run < NJOBS; run++))
       do
       export NAME="pyEmb_${run}"
		 export OUTPUTDIR="$BASEDIR/DataOut/pythia/jetonly/$TYPE/$run"
		if [ ! -e $OUTPUTDIR ]; then
         mkdir -p $OUTPUTDIR
       fi
 qsub -l projectio=1 -m n -N $NAME -e $LOGDIR -o $LOGDIR -V run_pythiaEmb.csh
done
done
done
