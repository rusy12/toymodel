#!/bin/bash
BASEDIR="$HOME/jet_analysis/toymodel"
LOGDIR="$BASEDIR/submitter/log"

kEVENTS=1000

export PTCUT=0.2
export MAXRAP=1.0
export CHARGED=1 #charged jets only
export EFFICORR=0 #apply tracking efficiency 
export PTSMEAR=0 #track pT smearing
export EFFTYPE="AuAu" #tracking efficiency model: pp | AuAu - like
export EFFPATH="$HOME/jet_analysis/STARJet/analysis/efficiency" #path to tracking efficiency files
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies, [-100%,100%]
export JETFRAG="g" #jet fragmentation: u | g
export DOSCALE=0 #rescale charge jets to the momentum of created full jet
export CENTRAL=1 #central or peripheral collisions
SUFFIX="_${JETFRAG}"

for WEIGHT in 0 1 # 0: flat, 1: pT^-5
do
export WEIGHT 
for RPARAM in 0.3 0.6 #0.3 0.4
do
export RPARAM

   if [ $RPARAM == "0.2" ]; then
      ACUT=0.07
   fi
   if [ $RPARAM == "0.3" ]; then
      ACUT=0.2
   fi
   if [ $RPARAM == "0.4" ]; then
      ACUT=0.4
   fi
   if [ $RPARAM == "0.5" ]; then
      ACUT=0.6
   fi
   if [ $RPARAM == "0.6" ]; then
      ACUT=0.8
   fi


export AREACUT=$ACUT
echo "Area cut: $AREACUT"

	if [ $EFFICORR -eq 1 ]; then
   TYPE1="_effcorr"
   else
   TYPE1=""
   fi

   if [ $PTSMEAR -eq 1 ]; then
   TYPE2="_pTsmear"
   else
   TYPE2=""
   fi

	if [ $CENTRAL -eq 0 ]; then
	CENTRALITY="_peripheral"
	else
	CENTRALITY=""
	fi

TYPE="pyTEST_${kEVENTS}k_charged_R${RPARAM}${TYPE1}${TYPE2}${CENTRALITY}${SUFFIX}"

export NEVENTS=100000 #number of events per job
   START=0
   export NJOBS=$(( kEVENTS * 1000 / NEVENTS ))
   #START=75
   #NJOBS=100
   echo "number of jobs: $NJOBS"

for((run=START; run < NJOBS; run++))
       do
       export NAME="pyTest_${run}"
		 export OUTPUTDIR="$BASEDIR/DataOut/pythia/jetonly/$TYPE/$run"
		if [ ! -e $OUTPUTDIR ]; then
         mkdir -p $OUTPUTDIR
       fi
 qsub -l projectio=1 -m n -N $NAME -e $LOGDIR -o $LOGDIR -V run_pythiaTest.sh
done
done
done
