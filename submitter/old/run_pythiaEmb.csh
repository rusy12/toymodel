#!/bin/csh

starver SL15e
source ../set_paths.csh


set BASEDIR="$HOME/jet_analysis/toymodel"
set MacroDir="$BASEDIR/macros"

setenv INPUTDIR $OUTPUTDIR
setenv WRKDIR $OUTPUTDIR

cd $MacroDir

root -b -q -l run_pythiaEmb.C
