#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load toymodel/toymodel

BASEDIR=$HOME/jet_analysis/toymodel
MacroDIR=$BASEDIR/macros/unfolding

prior_type=(measured flat pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)

for PTTHRESH in `echo $PTLEADCUTS`
do
   export PTTHRESH
for PRIOR in `echo $PRIORS`
do
   export PRIOR
	OUT_DIR=$DATA_PATH"/Unfolded_R${RPARAM}_${UNFTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA_TOY}${DIRSUF}/"${prior_type[$PRIOR]}
   #echo "creating directory: $OUT_DIR"
   #rm  $OUT_DIR/*.root 
   mkdir -p $OUT_DIR
   export OUT_DIR

	cd $MacroDIR
	root -b -l -q unfold_roounfold_uneqbin.C

	#if [ $NBINS == "VAR" ]; then
	#	root -b -l -q unfold_roounfold_uneqbin.C
	#else
	#	root -b -l -q unfold_roounfold.C
	#fi

done
done
