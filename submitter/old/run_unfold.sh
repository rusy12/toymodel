#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load toymodel/toymodel

BASEDIR=$HOME/jet_analysis/toymodel
MacroDIR=$BASEDIR/macros/unfolding


cd $MacroDIR
if [ $NBINS == "VAR" ]; then
  root -b -l -q unfold_roounfold_uneqbin.C
else
  root -b -l -q unfold_roounfold.C
fi

