#!/bin/bash
#source  /home/users/startup/pdsf.bashrc
source  $HOME/privatemodules/pdsf.bashrc

module load use.own
module load toymodel/toymodel


BASEDIR=$HOME/jet_analysis/toymodel
MacroDir=$BASEDIR/macros

cd $MacroDir
root -b -q -l run_pythiaEmb.C
