
TH1D* rebinhisto(TH1D* hOLD, TH1D* hTEMPLATE, TString name)
{	
      Double_t yield[10];
      Double_t error[10];
      TH1D* hNEW=(TH1D*) hTEMPLATE->Clone(name.Data());
      hNEW->Reset("MICE");
      for(int bnvar=1;bnvar<hNEW->GetNbinsX()+1;bnvar++){
	int bincount=0;
	for(int bn=1; bn<hOLD->GetNbinsX()+1;bn++){
	  if(hNEW->FindBin(hOLD->GetBinCenter(bn))!=bnvar)continue;
	  //cout<<"bin new "<<bnvar<<"bin old "<<bn<<endl;
	  yield[bincount]=hOLD->GetBinContent(bn);
	  error[bincount]=hOLD->GetBinError(bn);
	  bincount++;
	}//loop over bins of original histogram
	
	double yieldt=0;
	double errt2=0;
	for(int i=0;i<bincount;i++)
	{
	  yieldt=yieldt+yield[i];
	  errt2=error[i]*error[i];
	}
	hNEW->SetBinContent(bnvar,yieldt/bincount);
	hNEW->SetBinError(bnvar,TMath::Sqrt(errt2)/bincount);
      }//loop over bins of new histogram 
  return hNEW;
}

void make_htemplate(){
  Float_t pTrange=100; //pTrange in input histograms (has to be same in all histos!)
  const Int_t newbins=62;
  Double_t pTbinArray[newbins];
  pTbinArray[0]=-pTrange;
  Double_t width=20;
  for(int i=1; i<=newbins; i++){
   if(pTbinArray[i-1]==-60) width=10;
   else if(pTbinArray[i-1]==-30) width=5;
   else if(pTbinArray[i-1]==-15) width=2;
   else if(pTbinArray[i-1]==-5) width=  Float_t pTrange=100; //pTrange in input histograms (has to be same in all histos!)
  const Int_t newbins=62;
  Double_t pTbinArray[newbins];
  pTbinArray[0]=-pTrange;
  Double_t width=20;
  for(int i=1; i<=newbins; i++){
   if(pTbinArray[i-1]==-60) width=10;
   else if(pTbinArray[i-1]==-30) width=5;
   else if(pTbinArray[i-1]==-15) width=2;
   else if(pTbinArray[i-1]==-5) width=1;
   else if(pTbinArray[i-1]==-1) width=0.5;
   else if(pTbinArray[i-1]==10) width=1;
   else if(pTbinArray[i-1]==20) width=2;
   else if(pTbinArray[i-1]==30) width=5;
   else if(pTbinArray[i-1]==50) width=10;
   else if(pTbinArray[i-1]==80) width=20;
   pTbinArray[i]=pTbinArray[i-1]+width;
//if(pTbinArray[i]==pTrange)cout<<"lastbin:"<<i<<endl;
  }
cout<<"binning:"<<endl;
for(int i=1; i<=newbins; i++){
cout<<i<<": "<<pTbinArray[i-1]<<" - "<<pTbinArray[i]<<endl;}
1;
   else if(pTbinArray[i-1]==-1) width=0.5;
   else if(pTbinArray[i-1]==10) width=1;
   else if(pTbinArray[i-1]==20) width=2;
   else if(pTbinArray[i-1]==30) width=5;
   else if(pTbinArray[i-1]==50) width=10;
   else if(pTbinArray[i-1]==80) width=20;
   pTbinArray[i]=pTbinArray[i-1]+width;
//if(pTbinArray[i]==pTrange)cout<<"lastbin:"<<i<<endl;
  }
cout<<"binning:"<<endl;
for(int i=1; i<=newbins; i++){
cout<<i<<": "<<pTbinArray[i-1]<<" - "<<pTbinArray[i]<<endl;}
}
