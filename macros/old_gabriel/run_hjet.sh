#!/bin/bash

THRMSOFT="$HOME/toymodel"

export OUTPUTDIR="../DataOut/hjet"
export WRKDIR="../DataOut/hjet/"
export INPUTDIR="../DataOut/hjet"

export PTCUT=0.2
export RADIUS=0.4


export RECOILSPECTRUM="${THRMSOFT}/Production/recoil_rhic.root"

export NEVENTS=1E4

export PTTRIGMIN=5.0
export PTTRIGMAX=10.0

export MULTIPLICITY=2000
export SIGMA_MULTIPLICITY=0

export BKGDPTCUT=0.2

export PTTHRESH=3.0

export COLLIDER=RHIC

mkdir -p $OUTPUTDIR

root -l -b produce_hjet_data.C -q
root -l -b -q make_dNdpT_hjet.C
root -l -b rec_jets.C -q
root -l -b make_ntuple.C -q
root -b -q -l make_histos.C
