#!/bin/bash

THRMSOFT="$HOME/Doutorado/toymodel/software"

export OUTPUTDIR="../DataOut/hjet_pythia"
export INPUTDIR="../DataOut/hjet_pythia"
export WRKDIR="../DataOut/hjet_pythia"

export RECOILSPECTRUM="${THRMSOFT}/Production/recoil_rhic.root"

export NEVENTS=1E4

export PTTRIGMIN=5.0
export PTTRIGMAX=10.0

export COLLIDER=RHIC
export MULTIPLICITY=2000
export SIGMA_MULTIPLICITY=0
export PTTHRESH=3.0

export ACUT=0.4
export PTCUT=0.2
export RADIUS=0.4
export BKGDPTCUT=0.2

mkdir -p $OUTPUTDIR

root -l -b produce_hjet_pythia.C -q

root -b -q -l make_dNdpT.C

root -b -q -l rec_jets.C

root -b -q -l make_hjet_ntuple.C

root -b -q -l make_histos.C

