void produce_hjet_data()
{
  TStopwatch timer;
  timer.Start();

  TString outdir = gSystem->Getenv("OUTPUTDIR");

  TString collider = gSystem->Getenv("COLLIDER");

  TString recoilspectrum = gSystem->Getenv("RECOILSPECTRUM");

  Int_t nevts = atof(gSystem->Getenv("NEVENTS"));

  Int_t mult = atoi(gSystem->Getenv("MULTIPLICITY"));
  Int_t sigmaMult = atoi( gSystem->Getenv("SIGMA_MULTIPLICITY"));

  Float_t pTtrigMin = atof(gSystem->Getenv("PTTRIGMIN"));
  Float_t pTtrigMax = atof(gSystem->Getenv("PTTRIGMAX"));

  cout << Form("Nevts = %d \t Mult = %d +/- %d \t %.1lf < pTtrig < %.1lf", nevts, mult, sigmaMult, pTtrigMin, pTtrigMax) << endl;

  ThrmHadronJetSim *sim = new ThrmHadronJetSim(outdir, pTtrigMin, pTtrigMax);

  sim->SetNevents(nevts);

  sim->SetMultiplicity(mult);

  sim->SetSigmaMultiplicity(sigmaMult);

  sim->SetKinematics(collider);

  sim->SetRecoilSpectrum(recoilspectrum);
  
  sim->Run();
    
  timer.Stop();
  timer.Print();
}
