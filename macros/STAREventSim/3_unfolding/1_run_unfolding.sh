#!/bin/bash
source ./set_paths.sh
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (BG_sp | BGD | dete | effi)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BGD effi
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

prior_type=(flat pythiadete pythia powlaw4 powlaw45 powlaw5 powlaw55 tsalis_1 tsalis_2 tsalis_3 tsalis_4 tsalis_5 tsalis_6 tsalis_7 tsalis_8 tsalis_9)
JET_TYPE="pythia" #pythia | sp 
export SVD=0 # SVD unfolding instead of Bayes
export SMOOTH=0 #smooth unfolded solutions in between iterations
#export NBINS=200
export NBINS="VAR"
export NITER=8 #number of iterations

# FOR PRIOR DISTRIBUTION
export PTCUT=0.2
export BINCONTCUT=10 #minimal bin content in measured distribution
export SECONDUNFOLD=0 #unfold already unfolded results (eg. using a different RM)
	export INPUTITER=4 #if unfolding already unfolded results, which iteration to unfold
#export PTCUTOFF=0 #from witch pT to start with unfolding
EFFICORR=1 # do efficiency correction
OSUFF="_GPC2" #output directory suffix
EFFI_UNFOLD=0 #unfold detector level jet spectrum for jet reco efficiency calculation
BININGS="1" #1 2 3 4" #different binnings
export USE2DHISTO=1 # is the histogram with the measured distribution a 2D histogram?

if [ $RMATRIX_TYPE == "effi" ]; then
	EFFI_UNFOLD=1
	BININGS="1"
fi
export EFFI_UNFOLD

CENTRAL=1 #central/peripheral collisons
if [ $CENTRAL -eq 1 ]; then
	CENTSUFF="_central"
	PTLEAD_MIN=5
	PTLEAD_MAX=7
	RAA=0.4 #RAA value used for generating the simulated spectrum - used only for desctription
else
	CENTSUFF="_peripheral"
	PTLEAD_MIN=4
	PTLEAD_MAX=7
	RAA=0.7 #RAA value used for generating the simulated spectrum - used only for desctription
fi
export PTLEAD_MIN #start with this pTlead cut
export PTLEAD_MAX #finish with this pTlead cut

for RPARAM in 0.3 #0.3 0.4
do
export RPARAM

SYSSUFF="_normal"

TOYMODELPATH="$TOYMODELDIR"
WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jet_plus_bg/charged_R${RPARAM}$CENTSUFF"
if [ ${EFFI_UNFOLD} -eq 1 ]; then
	WRKDIR="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/pyEmb_R${RPARAM}$CENTSUFF$SYSSUFF"
fi
export PRIOR_PATH="$ANALYSISDIR/out/prior"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH="$WRKDIR/rmatrix"
export EPSILON_PATH="$TOYMODELPATH/DataOut/$JET_TYPE/jetonly/pyEmb_R${RPARAM}$CENTSUFF$SYSSUFF" #path to jet reconstruction efficiency files
export RMATRIX_TYPE

export PRIOR_MIN=2 #start with this prior function
export PRIOR_MAX=15 #finish with this prior function
export PRIOR_SKIP1=3 #skip this prior function
export PRIOR_SKIP2=3
export PRIOR_SKIP3=3


if [ $EFFI_UNFOLD -eq 1 -o $RMATRIX_TYPE == "BG_sp" ]; then
	export EFFICORR=0 
else
	export EFFICORR
fi
if [ $EFFICORR -eq 0 ]; then
EFFSUF=""
else
EFFSUF="_eff"
fi

if [ $SVD -eq 0 ]; then
UTYPE="Bayes"
else
UTYPE="SVD"
fi

for BININGCH in `echo $BININGS`
do
export BININGCH 

OUT_DIR=$WRKDIR"/Unfolded_R${RPARAM}_${UTYPE}_${NBINS}bins_bining${BININGCH}_${RMATRIX_TYPE}_RAA${RAA}${OSUFF}"
echo "creating directory: $OUT_DIR"
#we will create a subdirectory for each prior
for PRIOR in 2 4 5 6 7 8 9 10 11 12 13 14 15 #0: truth, 1: flat, 2: biased pythia, 3: pT^(-3), 4:pT^(-4) 5: pT^(-5) 6:pT^(-6) 7: levy I 8: levy II
do
	mkdir -p $OUT_DIR/${prior_type[$PRIOR]}
done
export OUT_DIR

root -b -l -q run_unfolding.C

done #bining
done #R
