#!/bin/bash

COLLIDER="RHIC" # "LHC" # 

# FOR PRIOR DISTRIBUTION
export PTCUT=0.2

WRKDIR="~/Doutorado/toymodel/data/${COLLIDER}/inclusive/pythia/full"
export PRIOR_PATH="~/Doutorado/toymodel/data/${COLLIDER}/inclusive/pythia/jet"
export DATA_PATH=$WRKDIR
export RMATRIX_PATH=$WRKDIR

cd ~/Doutorado/toymodel/software/macros

for PTTHRESH in 2.0 3.0 4.0 5.0 6.0 # 7.0 8.0 10.0 15.0 20.0 25.0 #
  do
  export PTTHRESH
  root -b -l -q ../Unfolding/AnaMacros/unfold_toymodel_pythia.C
done
