#!/bin/bash

export RADIUS=0.3
export ACUT=0.2
export PTCUT=0.2
export WRKDIR="$TOYMODELDIR/DataOut/test"

root -l -b make_histos_deltapT.C -q

