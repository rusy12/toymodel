void make_ntuple()
{
  gSystem->Load("$TOYMODELDIR/Production/libThrm.so");
  gSystem->Load("$TOYMODELDIR/Analysis/libThrmAna.so");
  gSystem->Load("$TOYMODELDIR/Unfolding/libtoyUnfold.so");
  TStopwatch timer;
  timer.Start();

  TString sAcut = gSystem->Getenv("ACUT");
  TString spTcut = gSystem->Getenv("PTCUT");
  TString sradius = gSystem->Getenv("RADIUS");
  TString wrkdir = gSystem->Getenv("WRKDIR");
  Int_t useScratch = atoi(gSystem->Getenv("USESCRATCH"));
  if(useScratch) wrkdir = gSystem->Getenv("SCRATCH");

  Double_t Acut = sAcut.Atof();
  Double_t pTcut = spTcut.Atof();
  Double_t radius = sradius.Atof();
  
  ThrmAnaJetData *analysis = new ThrmAnaJetData(wrkdir, pTcut, radius);
  analysis->SetAcut(Acut);
  analysis->RunAnalysis();
  delete analysis;

  timer.Stop();
  timer.Print();
}
