#!/bin/bash

source set_paths.sh
WPATH="$TOYMODELDIR/DataOut/test/"
#for((INDEX=0; INDEX < 100; INDEX++))
#do
#WPATH="/global/project/projectdirs/star/starprod/picodsts/rusnak_tmp/tmp_ana/toymodel_out/pythia/jet_plus_bg/10M_charged_R0.2_pyjet_effcorr/$INDEX"
#WPATH="/global/project/projectdirs/star/starprod/picodsts/rusnak_tmp/tmp_ana/toymodel_out/pythia/jet_plus_bg/10M_charged_R0.2_pyjet_effcorr/52"
export WRKDIR=$WPATH
export USESCRATCH=0
export NEVENTS=5000

export NBIN=984 # 1259 # 
#export NBIN=1259 
export SIGMA_NBIN=0
export MULTIPLICITY=650 #2000 # 4800 #
#export MULTIPLICITY=2000
export SIGMA_MULTIPLICITY=0
export COLLIDER="RHIC_CHARGED" # RHIC # LHC # 
#export COLLIDER="RHIC"   

export ACUT=0.2
#export ACUT=0.0
export PTCUT=0.2
export PTMAX=30
export RADIUS=0.3
export BKGDPTCUT=0.2
export PTMINHARD=4 #start of the hard distribution, default: 3GeV/c

export CENTRAL=1 #central or peripheral collisions
export JETONLY=1 #only hard jet distribution
export BOLTZMANN=0 #only background
export HARDJET_TYPE=2 #use pythia jets with pTleading cut as the hard jet distribution
export EFFICORR=0 #apply tracking efficiency 
export PTSMEAR=1 #track pT smearing
export RAA=0.3
export RAAPTDEP=0 #pT dependent RAA
export MEANPT=600 #Background <pT> [MeV/c]

export EFFTYPE="AuAu" #tracking efficiency model: pp | AuAu - like
#export EFFPATH="$ANALYSISDIR/macros/efficiency" #path to tracking efficiency files
export JETFRAG="g" #jet fragmentation: u | g
export EFF_INCREMENT=0 #increase/decrease tracking efficiency for systematic studies
export DOSCALE=1 #rescale charge jets to the momentum of created full jet

export TOFEFFI=0 #apply TOF efficiency as one of the detector effects (this is used only for pp RM production)
export CUTSET=0 #cutset used for tracking efficiency calculation (0=primary tracks,1=primary tracks,2=global tracks)
export MOMRES=1 #TPC momentum resolution; 0: sigma=0.01*pT^2 (global tracks) 1: sigma=0.005*pT^2 (primary tracks, simple) 2: sigma=a+b*pT+c*pT^2 (primary tracks, more accurate)


root -l -q -b run_pythia.C 
#root -b -q -l make_dNdpT.C
#root -b -q -l rec_jets.C
#root -b -q -l make_ntuple.C
#root -b -q -l make_histos.C
#root -q -b -l make_histos_deltapT.C
#done
