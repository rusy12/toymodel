#!/bin/bash
SCRIPT_NAME=`basename -- $0`
_usage() {
    echo "Usage: ${SCRIPT_NAME} RMATRIX_TYPE (deltapT | BG_dete)" 
    exit 1
}

RMATRIX_TYPE=$1  #BG_sp BG_pyt dete BG_dete - correction for BG (using single particle / pythia jet), detector effects, BG+detector effects
echo "TYPE: $RMATRIX_TYPE"

#check arguments
[ -n "$RMATRIX_TYPE" ] || _usage

source ./set_paths.sh
export RPARAM=0.2
export RMATRIX_TYPE
JETTYPE="pythia"
#JETTYPE="sp"
SUFF="_normal"

CENTRAL=1
if [ $CENTRAL -eq 1 ]; then
CENTSUFF="_central"
#PTLEADCUTS="5 6 7"
else
CENTSUFF="_peripheral"
#PTLEADCUTS="4 5 6 7"
fi

export PATH_TO_DELTA_PT_HISTOGRAMS="$TOYMODELDIR/DataOut/$JETTYPE/jet_plus_bg/charged_R${RPARAM}$CENTSUFF"
export RM_PATH="$PATH_TO_DELTA_PT_HISTOGRAMS/rmatrix"
export PYEMB_PATH="$TOYMODELDIR/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${CENTSUFF}${SUFF}"
export PRIOR_PATH="$ANALYSISDIR/out/prior"

export PTLEAD=5
export NEVENTS=3000000

export PRIOR_START=2
export PRIOR_STOP=4
export PRIOR_SKIP1=3
export PRIOR_SKIP2=3
export PRIOR_SKIP3=3


#for PTLEAD in `echo $PTLEADCUTS`
#do
#export PTLEAD
	root -l buildResponseROO.C -q -b
#done
