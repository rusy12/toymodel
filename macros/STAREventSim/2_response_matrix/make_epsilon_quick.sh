#!/bin/bash
for CENTRAL in 0 #0-peripheral 1-central 2-pp
do
if [ $CENTRAL -eq 1 ]; then
	SUFFIX="_central"
	PTLEADCUTS="5 6 7"
elif [ $CENTRAL -eq 2 ]; then
	SUFFIX="_pp"
	PTLEADCUTS="0 1 2 3 4 5 6 7"
else
	SUFFIX="_peripheral"
	PTLEADCUTS="2 3" #4 5 6 7"
fi
export SUFFIX

for SYSSUF in "_normal" "_g" "_u" "_m5" "_p5" "_pp" "_trcuts2" #"_global" # "_AuAu" #"_tofp5" "_tofm5"  
do
export SYSSUF


for RPARAM in 0.2 0.3 0.4 #0.5 
do
export RPARAM

export DATAPATH="$HOME/jet_analysis/toymodel/DataOut/pythia/jetonly/pyEmb_R${RPARAM}${SUFFIX}${SYSSUF}"
mkdir -p "$DATAPATH/epsilon"

for PTTHRESH in `echo $PTLEADCUTS`
   do
   export PTTHRESH
   root -l -b make_epsilon_quick.C -q
done #pTlead
done #R
done #systematics suffix
done
